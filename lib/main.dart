import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:function_tree/function_tree.dart';
import 'package:audiofileplayer/audiofileplayer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Calculadora Switch',
      home: CalculadoraSwitch(),
    );
  }
}

const Color colorDark = Color(0xFF374352);
const Color colorLight = Color(0xFFE6EEFF);
const Color colorVerde = Color(0xFF40c4b0);
const Color colorRojo = Colors.redAccent;

double operacionInt = 0;
double historialInt = 0;
String operacionText = '';
String historialText = '';
String Ans = '';

String ecuacion = '0';
String resultado = '0';
String expresion = '0';
Audio audio = Audio.load('assets/audio.mp3');
String error = '';

class CalculadoraSwitch extends StatefulWidget {
  @override
  _CalculadoraSwitchState createState() => _CalculadoraSwitchState();
}

class _CalculadoraSwitchState extends State<CalculadoraSwitch> {
  botonPresionado(String textoBoton) {
    setState(() {
      audio.play();
      if (textoBoton == 'C') {
      } else if (textoBoton == '⌫') {
        if (historialText != null && historialText.length > 0) {
          historialText = historialText.substring(0, historialText.length - 1);
        }
      } else if (textoBoton == '0') {
        historialText = historialText + '0';
      } else if (textoBoton == '1') {
        historialText = historialText + '1';
      } else if (textoBoton == '2') {
        historialText = historialText + '2';
      } else if (textoBoton == '3') {
        historialText = historialText + '3';
      } else if (textoBoton == '4') {
        historialText = historialText + '4';
      } else if (textoBoton == '5') {
        historialText = historialText + '5';
      } else if (textoBoton == '6') {
        historialText = historialText + '6';
      } else if (textoBoton == '7') {
        historialText = historialText + '7';
      } else if (textoBoton == '8') {
        historialText = historialText + '8';
      } else if (textoBoton == '9') {
        historialText = historialText + '9';
      } else if (textoBoton == '.') {
        historialText = historialText + '.';
      } else if (textoBoton == '+') {
        historialText = historialText + '+';
      } else if (textoBoton == '-') {
        historialText = historialText + '-';
      } else if (textoBoton == 'x') {
        historialText = historialText + '*';
      } else if (textoBoton == '/') {
        historialText = historialText + '/';
      } else if (textoBoton == '(') {
        historialText = historialText + '(';
      } else if (textoBoton == ')') {
        historialText = historialText + ')';
      } else if (textoBoton == 'c') {
        historialText = '';
        operacionText = '';
      } else if (textoBoton == 'sin') {
        historialText = historialText + 'sin(';
      } else if (textoBoton == 'cos') {
        historialText = historialText + 'cos(';
      } else if (textoBoton == 'tan') {
        historialText = historialText + 'tan(';
      } else if (textoBoton == 'ANS') {
        historialText = historialText + Ans;
      } else if (textoBoton == '=') {
        try {
          operacionInt =
              double.parse(historialText.interpret().toStringAsFixed(2));
          operacionText = operacionInt.toString();
          historialText = operacionText;
          Ans = operacionText;
        } on Exception catch (_) {
          operacionText = 'Syntax ERROR';
        }
      }
    });
  }

  bool darkMode = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: darkMode ? colorDark : colorLight,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(18),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            setState(() {
                              darkMode ? darkMode = false : darkMode = true;
                            });
                          },
                          child: _switchMode()),
                      SizedBox(
                        height: 80,
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          '$operacionText',
                          style: TextStyle(
                              fontSize: 55,
                              fontWeight: FontWeight.bold,
                              color: darkMode ? Colors.white : colorRojo),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '=',
                            style: TextStyle(
                                fontSize: 35,
                                color: darkMode ? colorVerde : colorRojo),
                          ),
                          Text(
                            '$historialText',
                            style: TextStyle(
                                fontSize: 20,
                                color: darkMode ? colorVerde : Colors.grey),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              botonPresionado('sin');
                            },
                            child: _botonOvalo(title: 'sin'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('cos');
                            },
                            child: _botonOvalo(title: 'cos'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('tan');
                            },
                            child: _botonOvalo(title: 'tan'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('ANS');
                            },
                            child: _botonOvalo(title: 'ANS'),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              botonPresionado('c');
                            },
                            child: _buttonRounded(
                                title: 'C',
                                textColor: darkMode ? colorVerde : colorRojo),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('(');
                            },
                            child: _buttonRounded(title: '('),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado(')');
                            },
                            child: _buttonRounded(title: ')'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('/');
                            },
                            child: _buttonRounded(
                                title: '/',
                                textColor: darkMode ? colorVerde : colorRojo),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              botonPresionado('7');
                            },
                            child: _buttonRounded(title: '7'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('8');
                            },
                            child: _buttonRounded(title: '8'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('9');
                            },
                            child: _buttonRounded(title: '9'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('x');
                            },
                            child: _buttonRounded(
                                title: 'x',
                                textColor: darkMode ? colorVerde : colorRojo),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              botonPresionado('4');
                            },
                            child: _buttonRounded(title: '4'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('5');
                            },
                            child: _buttonRounded(title: '5'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('6');
                            },
                            child: _buttonRounded(title: '6'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('-');
                            },
                            child: _buttonRounded(
                                title: '-',
                                textColor: darkMode ? colorVerde : colorRojo),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              botonPresionado('1');
                            },
                            child: _buttonRounded(title: '1'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('2');
                            },
                            child: _buttonRounded(title: '2'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('3');
                            },
                            child: _buttonRounded(title: '3'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('+');
                            },
                            child: _buttonRounded(
                                title: '+',
                                textColor: darkMode ? colorVerde : colorRojo),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              botonPresionado('0');
                            },
                            child: _buttonRounded(title: '0'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('.');
                            },
                            child: _buttonRounded(title: '.'),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('⌫');
                            },
                            child: _buttonRounded(
                                icon: Icons.backspace_outlined,
                                iconColor: darkMode ? colorVerde : colorRojo),
                          ),
                          GestureDetector(
                            onTap: () {
                              botonPresionado('=');
                            },
                            child: _buttonRounded(
                                title: '=',
                                textColor: darkMode ? colorVerde : colorRojo),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ]),
        ),
      ),
    );
  }

  Widget _buttonRounded(
      {String title,
      double padding = 17,
      IconData icon,
      Color iconColor,
      Color textColor}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: CustomContainer(
        darkMode: darkMode,
        borderRadius: BorderRadius.circular(40),
        padding: EdgeInsets.all(padding),
        child: Container(
          width: padding * 2,
          height: padding * 2,
          child: Center(
            child: title != null
                ? Text(
                    '$title',
                    style: TextStyle(
                        color: textColor != null
                            ? textColor
                            : darkMode
                                ? Colors.white
                                : Colors.black,
                        fontSize: 30),
                  )
                : Icon(
                    icon,
                    color: iconColor,
                    size: 30,
                  ),
          ),
        ),
      ),
    );
  }

  Widget _botonOvalo({String title, double padding = 17}) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: CustomContainer(
        darkMode: darkMode,
        borderRadius: BorderRadius.circular(50),
        padding:
            EdgeInsets.symmetric(horizontal: padding, vertical: padding / 2),
        child: Container(
          width: padding * 2,
          child: Center(
            child: Text(
              '$title',
              style: TextStyle(
                  color: darkMode ? Colors.white : Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }

  Widget _switchMode() {
    return CustomContainer(
      darkMode: darkMode,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      borderRadius: BorderRadius.circular(40),
      child: Container(
        width: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Icon(
              Icons.wb_sunny,
              color: darkMode ? Colors.grey : colorRojo,
            ),
            Icon(
              Icons.nightlight_round,
              color: darkMode ? colorVerde : Colors.grey,
            ),
          ],
        ),
      ),
    );
  }
}

class CustomContainer extends StatefulWidget {
  final bool darkMode;
  final Widget child;
  final BorderRadius borderRadius;
  final EdgeInsetsGeometry padding;

  CustomContainer(
      {this.darkMode = false, this.child, this.borderRadius, this.padding});

  @override
  _CustomContainerState createState() => _CustomContainerState();
}

class _CustomContainerState extends State<CustomContainer> {
  bool _isPressed = false;

  void _onPointerUp(PointerUpEvent event) {
    setState(() {
      _isPressed = false;
    });
  }

  void _onPointerDown(PointerDownEvent event) {
    setState(() {
      _isPressed = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool darkMode = widget.darkMode;
    return Listener(
      onPointerUp: _onPointerUp,
      onPointerDown: _onPointerDown,
      child: Container(
        padding: widget.padding,
        decoration: BoxDecoration(
            color: darkMode ? colorDark : colorLight,
            borderRadius: widget.borderRadius,
            boxShadow: _isPressed
                ? null
                : [
                    BoxShadow(
                      color:
                          darkMode ? Colors.black54 : Colors.blueGrey.shade200,
                      offset: Offset(4.0, 4.0),
                      blurRadius: 15.0,
                      spreadRadius: 1.0,
                    ),
                    BoxShadow(
                      color: darkMode ? Colors.blueGrey.shade700 : Colors.white,
                      offset: Offset(-4.0, -4.0),
                      blurRadius: 15.0,
                    )
                  ]),
        child: widget.child,
      ),
    );
  }
}
